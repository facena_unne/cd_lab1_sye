(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ngApp.factory('AnalyticsFactory', [function () {
        
    var pub = {};
    var _alreadySetteddUserFlag = false;//previene que se sette dos veces el USER_ID.
    pub.lastestPage = "landing";
    
    
    pub.setUserId=function(p_nick){
        if( !_alreadySetteddUserFlag ){
            _alreadySetteddUserFlag = true;
            ga('set', 'userId', p_nick);             
        }        
    };
    
    pub.pageView=function(p_page){	        
        ga('send', {
            'hitType': 'pageview',
            'page': p_page,
            'title': 'lab_cd'
        });          
    };
        
    pub.event=function(p_category, p_action, p_label, p_value){      
        if(typeof p_value === "value"){p_value= 0;}              
       				
        ga('send', 'event', {
            'eventCategory': p_category,
            'eventAction': p_action,
            'eventLabel':p_label,
            'eventValue':p_value
          });
    };

    pub.init=function(p_clientId, p_callback){
        var _data= 'auto';
        if (typeof p_clientId !== 'undefined' && p_clientId !== null){
            _data = {'clientId': p_clientId};
        }
        ga('create', 'UA-168052318-1', _data);
        ga('set', 'hostname', window.location.protocol + '//' + window.location.hostname +window.location.pathname + window.location.search);
        if (typeof p_callback === 'function'){p_callback();}
    };
    return pub;
    
}]);