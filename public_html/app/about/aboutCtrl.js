/** * 
 *  Controlador  de Angular 
 *  UTF-8 31/05/2020 
 *  about
 *  Pablo N. Garcia Solanellas
 *  Corrientes, Argentina.
 **/

controllers.controller('AboutCtrl', 
['$scope', 'AnalyticsFactory',
function ($scope, AnalyticsFactory) {
//VARIABLES PRIVADAS*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
        var _im = {
            caption: 'About',
            name: 'about',
            alreadyInitFlag: false,
            pub: {
                view: {},
                views: {
                    list: [
                        {id: 0, name: 'about', type: 'list', updatePath: false},
                        {id: 1, name: 'form', type: 'form', updatePath: true, smartSelect: [{'select2Form': 'id'}]}
                    ],
                    keys: {'about': 0, 'form': 1}
                }
            }
        };
        var _defaults = {};
        _defaults[_im.name] = {};

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//VARIALBES $scope*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-        
        $scope.forms[_im.name] = {};
        $scope.inEdit[_im.name] = angular.copy(_defaults[_im.name]);

        $scope[_im.name] = angular.copy(_im.pub);
        $scope[_im.name].view = $scope[_im.name].views.list[0];
        $scope.inner = {
            ORDEN_BY: {
                DIRECTION: "-",
                COLUM: "id"
            }
        };
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//FUNCRIONES $scope*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

    $scope.swAboutView = function (p_index, p_data) {
        $scope[_im.name].view = $scope[_im.name].views.list[p_index];
    };
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//FUNCRIONES privadas*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
//LAST FUNCTIONS init | preInit-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
       
    var _init = function () {
        
    };
    AnalyticsFactory.pageView("about");

}]);


