/**
 * controlador de la vista senial.
 * grafica una señal sen y genera un historial de variaciones de la señal.
 * @author Pablo N. Garcia Solanellas
 * @date: 2020 may
*/

controllers.controller('SenialesCtrl', 
['$scope', '$timeout','AnalyticsFactory',
function ($scope,$timeout,AnalyticsFactory) {
//VARIABLES PRIVADAS*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
        var _im = {
            caption: 'Seniales',
            name: 'seniales',
            requiredAssets: [""],
            alreadyInitFlag: false,
            pub: {
                view: {},
                views: {
                    list: [
                        {id: 0, name: 'seniales' },
                        {id: 1, name: 'seniales2'}
                    ],
                    keys: {'seniales': 0, 'seniales2': 1}
                }
            }
        };
        var _defaults = {};        
        _defaults[_im.name] = {
            amplitud:40,
            fase:10,
            frecuencia:40,
            armonicos:4,
            hertz:5
        };
        var _animacionId=undefined;                        
        var _lineWidth = 1.0;
        var step =4;
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//VARIALBES $scope*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-        
        $scope.forms[_im.name] = {};
        $scope.inEdit[_im.name] = angular.copy(_defaults[_im.name]);
        
        $scope[_im.name] = angular.copy(_im.pub);
        $scope[_im.name].view = $scope[_im.name].views.list[0];        
        $scope.animationOn=true;
        $scope.inner={
            canvasSenial : null,
            contextSenial : null,
            ctx:null,
            amplitudes :[],
            frecuencias:[],
            fases:[]
        };
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//FUNCRIONES $scope*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

    $scope.swSenialesView = function (p_index) {
        $scope[_im.name].view = $scope[_im.name].views.list[p_index];
    };
   
    $scope.refresh=function(){
        _animar( draw );
        _staticSpirograph();        
    };
    $scope.startStop=function(){        
        if (typeof _animacionId !== "undefined")
        {            
            _parar();
            $scope.animationOn=false;
        }
        else
        {
            _animar( draw );   
            $scope.animationOn=true;
        }
    };
    $scope.clearmultiplesSeniles =function () {            
        var canvas2 = document.getElementById("multiplesSeniles");
        var context = canvas2.getContext("2d");
        context.clearRect(0, 0, canvas2.width, canvas2.height);
        _ejesYreferencias(context);
        context.save();   
    };
    $scope.runAudio=function(){
        _audio();
    };
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//FUNCRIONES privadas*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    var _parar =function(){
        window.cancelAnimationFrame(_animacionId);
        _animacionId=undefined;
    };
    var _ejesYreferencias=function(p_ctx, ejes) {              
        //iniciamos/reiniciamos el espacio de trabajo (contexto)
        p_ctx.lineWidth = 1.0;
        p_ctx.beginPath();                
        //color de los ejes por defecto rojo.
        p_ctx.strokeStyle = "rgb(255,0,0)";        
        //Sobre el Eje - X                 
        _ejeX(p_ctx);
        
        //Sobre el EjeY
        _ejeY(p_ctx);
        p_ctx.stroke();
        p_ctx.save();
        
        _referenciaY(p_ctx);
        //dibujar 
        
    };
    
    var _ejeX=function (p_ctx){        
        p_ctx.font = "11px Arial";
        
        p_ctx.fillStyle = "yellow";
        p_ctx.lineWidth = _lineWidth;
        //enpiesa en 0 y a la mitad del canvas
        p_ctx.moveTo(0, p_ctx.canvas.height/2);
        // se extiende por todo el ancho hasta el otro extremo
        p_ctx.lineTo(p_ctx.canvas.width, p_ctx.canvas.height/2);
        p_ctx.fillText("+A", 3, 15);
        
            
        p_ctx.fillText("0", 5, (p_ctx.canvas.height-10 )/2);
        p_ctx.fillText("-A", 3, p_ctx.canvas.height-10);
        
        var _div=10;
        p_ctx.fillStyle = "red";
        for (var i = 1; i < _div; ++i) {
            p_ctx.moveTo(12, (p_ctx.canvas.height)/_div*i );
            p_ctx.lineTo(20, (p_ctx.canvas.height)/_div*i );
        }
        p_ctx.fillStyle = "yellow";
        p_ctx.font = "8px Arial";
        p_ctx.fillText("tiempo", p_ctx.canvas.width-35,( p_ctx.canvas.height/2) +10 );
    };
    
    var _ejeY=function (p_ctx){          
        p_ctx.moveTo(0, 0);
        p_ctx.lineTo(0, p_ctx.canvas.height);
    };
    var _referenciaY=function(p_ctx){
        
        p_ctx.beginPath();
        p_ctx.lineWidth = _lineWidth;
        p_ctx.strokeStyle = "rgb(255,0,0)";        
        var _refSize = 1;//p_ctx.canvas.width /  20 ;
        
        for (var i=0; i <= _refSize; i++){
            var _x = 20*i;
            p_ctx.moveTo(_x, 0);
            p_ctx.lineTo(_x, p_ctx.canvas.height);    
        }
        p_ctx.stroke();
        p_ctx.save();
        
    };       
    var plotSine=function(ctx, xOffset, yOffset, p_randonColor) {
        var width = ctx.canvas.width;
        var height = ctx.canvas.height;

        ctx.beginPath();
        ctx.lineWidth = _lineWidth;
        
        ctx.strokeStyle = "rgb(66,44,255)";
        if (p_randonColor){ctx.strokeStyle =_randomRGBCOLOR();  }

        var x = 0;// desfase de la linea vertical de referencia
        var y =0;
        
        var amplitude = $scope.inEdit[_im.name].amplitud;
        var frequency = $scope.inEdit[_im.name].frecuencia;        
        
        while (x < width) {
            
            y = height/2 + amplitude * Math.sin(
                    (x+xOffset) / (frequency - $scope.inner.frecuencias.length)
            );
            ctx.lineTo(x, y);
            x++;            
        }
        ctx.stroke();
        ctx.save();

        //drawPoint(ctx, y); // punto de referencia
        ctx.stroke();
        ctx.restore();
    };
    var draw = function() {
        // limpiamos el bastidor.
        $scope.inner.contextSenial.clearRect(0, 0, $scope.inner.canvasSenial.width, $scope.inner.canvasSenial.height);
        
        _ejesYreferencias($scope.inner.contextSenial);
        $scope.inner.contextSenial.save();            

        plotSine($scope.inner.contextSenial, step, 50);
        $scope.inner.contextSenial.restore();

        step += 4;
        _animar(draw);
        //;
    };
    
    var _animar =function(p_fn){
        _animacionId = window.requestAnimationFrame(p_fn);
    };
    
    
    var _staticSpirograph=function () {            
        var canvas2 = document.getElementById("multiplesSeniles");
        var context = canvas2.getContext("2d");

        _ejesYreferencias(context);
        context.save();
        
        plotSine(context, $scope.inEdit.seniales.fase, 0,true );
                
    };    
    
    var _randomRGBCOLOR =function(){
        var o = Math.round;
        var r = Math.round;
        var s = 255;
        return 'rgb(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ')';
    };
    //https://teropa.info/blog/2016/08/04/sine-waves.html    
    var _audio=function(){
        const REAL_TIME_FREQUENCY = $scope.inEdit[_im.name].frecuencia;
        //pasamos la frecuencia a radianes rad/s
        const ANGULAR_FREQUENCY = REAL_TIME_FREQUENCY * 2 * Math.PI;//rad/s

        let audioContext = new AudioContext();
        
        //https://developer.mozilla.org/en-US/docs/Web/API/BaseAudioContext/createBuffer
        //let buffer = baseAudioContext.createBuffer(numOfchannels, length, sampleRate);
        let _numOfchannels = 1;
        let _length = 88200;
        let _sampleRate=44100;
        let myBuffer = audioContext.createBuffer(_numOfchannels, _length, _sampleRate);
        let myArray = myBuffer.getChannelData(0);
        for (let sampleNumber = 0 ; sampleNumber < _length ; sampleNumber++) {
          myArray[sampleNumber] = generateSample(sampleNumber);
        }

        function generateSample(sampleNumber) {
          let currentTime = sampleNumber / _sampleRate; 
          let currentAngle = currentTime * ANGULAR_FREQUENCY;//rad/s
          return Math.sin(currentAngle);
        }

        let src = audioContext.createBufferSource();
        src.buffer = myBuffer;
        src.connect(audioContext.destination);
        src.start();  
        let _durecion = 2;//seconds
        src.stop(audioContext.currentTime + _durecion); 
    };

//LAST FUNCTIONS init | preInit-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
        var _init = function () {
            
            //$scope.msgboxManager('espere', _im.caption, 'Cargando...');
            if (!_im.alreadyInitFlag 
                && document.getElementById("multiplesSeniles")!== null
                && document.getElementById("senial")!== null) 
            {
                $scope.$watch("inEdit",_staticSpirograph,true);
                for(var i=1; i<=360;i++){                                        
                    $scope.inner.fases.push({ nro:i,label: i + "°" });
                }
                for(var i=1; i<=56;i++){                                        
                    $scope.inner.amplitudes.push( { nro:i,label: i + " " } );
                }
                for(var i=1; i<=128;i++){                                        
                    $scope.inner.frecuencias.push({ nro:i,label: i + " Hz" });
                }
                    
                _im.alreadyInitFlag = true;      
                AnalyticsFactory.pageView("seniales"); 
                
                $scope.inner.canvasSenial = document.getElementById("senial");
                $scope.inner.contextSenial = $scope.inner.canvasSenial.getContext("2d");
                
                $scope.refresh();
            }else{
                $timeout(_init,1000);
            }            
        };        
        $timeout(_init,100);        
    }]);


