/**
 * 
 * @author Pablo N. Garcia Solanellas
 */

var ngApp = angular.module('ngApp',[
    'controllers','ngRoute',
    'LocalStorageModule','filters'
]);  

var controllers = angular.module('controllers', []);
var filters = angular.module('filters', []);

ngApp.config(
    ['$routeProvider', 'localStorageServiceProvider',
    function( $routeProvider, localStorageServiceProvider ) {    
    
    localStorageServiceProvider.setPrefix('cd2020')
    .setStorageType('localStorage')
    .setNotify(true, true);
   
    
    $routeProvider         
        .when('/seniales', {
            templateUrl: 'app/seniales/mainView.html?c=TIME_STAMP_JS_FILE_HERE',
            controller: 'SenialesCtrl'
        })     
        .when('/fourier', {
            templateUrl: 'app/fourier/mainView.html?c=TIME_STAMP_JS_FILE_HERE',
            controller: 'FourierCtrl'
        })  
        .when('/about', {
            templateUrl: 'app/about/mainView.html?c=TIME_STAMP_JS_FILE_HERE',
            controller: 'AboutCtrl'
        })
        .otherwise({
            redirectTo: '/seniales'
        });   
}]);
