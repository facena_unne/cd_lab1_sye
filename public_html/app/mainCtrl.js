/* 
 *Controlador ppal
 *@author Pablo N. Garcia Solanellas
 */
controllers.controller('MainCtrl', 
['$scope','$timeout','$window','AnalyticsFactory',
function($scope, $timeout, $window,AnalyticsFactory ){       
    
    var COMMON_APP_URL = location.protocol+"//"+location.hostname;
    $scope.APP_VERSION = "TIME_STAMP_JS_FILE_HERE";        
    $scope.projectName = "cd";
    $scope.serverPrefix=location.protocol+"//"+location.hostname+":"+location.port+"/"+$scope.projectName+"/";           
    $scope.forms={};     
    $scope.inEdit = {};            
       
    //---------
    var _init=function(){
        angular.element(document).find("body").removeClass("hidden");
        AnalyticsFactory.init();   
        $timeout(_preInitAdmin,1000);        
    };    
    //iniciar
    _init();
    
    
}]);