/**
 * controlador de la vista fourier.
 * Grafica una senial en funcion de la suma de otras (armonicas).
 * @author Pablo N. Garcia Solanellas
 * @date: 2020 may
*/
controllers.controller('FourierCtrl', 
['$scope','$timeout','AnalyticsFactory', 
    function ($scope,$timeout,AnalyticsFactory) 
{
//VARIABLES PRIVADAS*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
        var _im = {
            caption: 'Fourier',
            name: 'fourier',
            requiredAssets: [""],
            alreadyInitFlag: false,
            pub: {
                view: {},
                views: {
                    list: [
                        {id: 0, name: 'fourier', fm: {caption: 'fourier', iclass: 'fa fa-check text-primary'}},
                        {id: 1, name: 'fourier2', fm: {caption: 'fourier2', iclass: 'fa fa-file'}}
                    ],
                    keys: {'fourier': 0, 'fourier2': 1}
                }
            }
        };
        var _defaults = {};
        _defaults[_im.name] = {
            armonicos:2,
            amplitud:3,
            frequency:-3.5
        };
        
        var waveformInput = document.getElementById("waveform").elements["waveform"];
        var PI2 = Math.PI * 2.0;
        var Scale = 64.0;
        var time = 0.0;
        var startTime = new Date().getTime();
        var values = [];
        var valuePointer = 0;
        var x = 128.0,y = 128.0;
        var canvas = document.querySelector("canvas");
        var context = canvas.getContext("2d");
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//VARIALBES $scope*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-        
        $scope.forms[_im.name] = {};
        $scope.inEdit[_im.name] = angular.copy(_defaults[_im.name]);

        $scope[_im.name] = angular.copy(_im.pub);
        $scope[_im.name].view = $scope[_im.name].views.list[0];
        $scope.lista ={
            armonicos: [],
            amplitudes:[]
        };
        
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//FUNCRIONES $scope*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

        $scope.swFourierView = function (p_index) {
            $scope[_im.name].view = $scope[_im.name].views.list[p_index];
        };

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//FUNCRIONES privadas*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

    
    var _drawArmonico=function(_armonico) {
        var phase = _armonico * time * PI2;
        var radius = $scope.inEdit.fourier.amplitud / (_armonico * Math.PI) * Scale;
        context.beginPath();
        context.lineWidth = 1.0;
        context.strokeStyle = "rgba(255,128,32,1.0)";
        context.arc(x, y, radius, 0, PI2);
        context.stroke();
        context.strokeStyle = "rgba(255,255,255,0.4)";
        context.moveTo(x, y);
        x += Math.cos(phase) * radius;
        y += Math.sin(phase) * radius;
        context.lineTo(x, y);
        context.stroke();
    };

    /**
     * Linea que une la grafica de los arminicos con la grafica de la señal     
     */
    var _drawLineDeUnion=function() {
        context.beginPath();
        context.moveTo(x + 0.5, y + 0.5);
        context.lineTo(256 + 0.5, y + 0.5);
        context.strokeStyle = "rgba(255,0,0,1.0)";
        context.stroke();
    };

    var drawOnda=function() {
       values[valuePointer++ & 511] = y;
       context.beginPath();
       context.strokeStyle = "rgba(0,255,0,1)";
       context.moveTo(256 + 0.5, y + 0.5);
       for (var i = 1; i < 512; ++i) {
           context.lineTo(256 + i + 0.5, values[(valuePointer - i) & 511] + 0.5);
       }
       context.stroke();
    };
    
    var _frame =function() {        
        canvas.width =$(".box-body").width();
        canvas.height = 256;
        
        x = 144.0;
        y = 128.0;
        switch (waveformInput.value) {
            case "square":
                for (var _armonicos = 0; _armonicos <= $scope.inEdit.fourier.armonicos; _armonicos++) {
                    _drawArmonico((_armonicos << 1) + 1);
                }
                break;
            case "sawtooth":
                for (var _armonicos = 1; _armonicos <= $scope.inEdit.fourier.armonicos; _armonicos++) {
                    _drawArmonico(_armonicos << 1);
                }
                break;
        }
        _drawLineDeUnion();
        drawOnda();
        
        //avanzamos en la gradica en sentido del relog.
        var now = new Date().getTime();
        time += (now - startTime) * Math.pow(10.0, $scope.inEdit.fourier.frequency);
        startTime = now;        
        window.requestAnimationFrame(_frame);
    };
//LAST FUNCTIONS init | preInit-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
        var _init = function () {
            //$scope.msgboxManager('espere', _im.caption, 'Cargando...');
            if (!_im.alreadyInitFlag 
                && document.getElementById("fourierCanvas") !==null) {
                _im.alreadyInitFlag = true;
                for(var i=1;i<2000;i++){                    
                    $scope.lista.armonicos.push({nro:i,label: (i+" Armonicos")});
                } 
                
                for(var i=1;i<=6;i++){                    
                    $scope.lista.amplitudes.push({value:i,label: (i+" adim")});
                }                 
                waveformInput = document.getElementById("waveform").elements["waveform"];
                canvas = document.getElementById("fourierCanvas");
                context = canvas.getContext("2d");
                _frame();
                AnalyticsFactory.pageView("fourier");
            }else{
                $timeout(_init,500);
            }            
        };
        $timeout(_init,0);
        


    }]);

filters.filter('fqHZ', 
['$filter', function($filter){    
    var pub =function( p_frecuencia ) {                             
                
        var _hz = ( parseFloat(p_frecuencia) + 4);
        return $filter("number")(_hz,0) ;
    };
    return pub;
}]);



